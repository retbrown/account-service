package org.bitbucket.retbrown.revolut.account.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.UUID;

public class Transfer {
    private UUID id;

    private UUID source;

    private UUID destination;

    private BigDecimal value;

    public Transfer() {
    }

    public Transfer(UUID id, UUID source, UUID destination, BigDecimal value) {
        this.id = id;
        this.source = source;
        this.destination = destination;
        this.value = value;
    }

    @JsonProperty
    public UUID getId() {
        return id;
    }

    @JsonProperty
    public UUID getSource() {
        return source;
    }

    @JsonProperty
    public UUID getDestination() {
        return destination;
    }

    @JsonProperty
    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Transfer{" +
                "id=" + id +
                ", source=" + source +
                ", destination=" + destination +
                ", value=" + value +
                '}';
    }
}
