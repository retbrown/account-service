package org.bitbucket.retbrown.revolut.account.db;

import org.bitbucket.retbrown.revolut.account.entity.Account;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class AccountDao {
    //This can be replaced with a full in memory database in larger example
    private Map<UUID, Account> accounts = populateAccounts();

    private static Map<UUID, Account> populateAccounts() {
        Map<UUID, Account> accountMap = new HashMap<>();

        accountMap.put(UUID.fromString("b923ad4f-0dca-46be-be67-afb69b722787"), new Account(UUID.fromString("b923ad4f-0dca-46be-be67-afb69b722787"), new BigDecimal(0.0)));
        accountMap.put(UUID.fromString("538978eb-cebe-4383-afd2-1310e569b0bd"), new Account(UUID.fromString("538978eb-cebe-4383-afd2-1310e569b0bd"), new BigDecimal(10.30)));
        accountMap.put(UUID.fromString("f203fcd4-e4e0-4922-afe5-8197b8bd3360"), new Account(UUID.fromString("f203fcd4-e4e0-4922-afe5-8197b8bd3360"), new BigDecimal(4000.39)));
        accountMap.put(UUID.fromString("ff1efe90-a166-4d2d-9a49-fd8968fa870f"), new Account(UUID.fromString("ff1efe90-a166-4d2d-9a49-fd8968fa870f"), new BigDecimal(134.29)));
        accountMap.put(UUID.fromString("8fc1c6d9-d72c-4ef3-a784-15418d4f15d7"), new Account(UUID.fromString("8fc1c6d9-d72c-4ef3-a784-15418d4f15d7"), new BigDecimal(100.00)));

        return accountMap;
    }

    public Optional<Account> findById(UUID id) {
        return Optional.ofNullable(accounts.get(id));
    }

    public Account update(Account account) {
        return accounts.put(account.getId(), account);
    }
}
