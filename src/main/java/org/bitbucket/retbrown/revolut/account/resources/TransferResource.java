package org.bitbucket.retbrown.revolut.account.resources;

import org.bitbucket.retbrown.revolut.account.db.AccountDao;
import org.bitbucket.retbrown.revolut.account.db.TransferDao;
import org.bitbucket.retbrown.revolut.account.entity.Account;
import org.bitbucket.retbrown.revolut.account.entity.Transfer;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.UUID;

@Path("/transfer")
@Produces(MediaType.APPLICATION_JSON)
public class TransferResource {

    private final AccountDao accountDao;
    private final TransferDao transferDao;

    public TransferResource(AccountDao accountDao, TransferDao transferDao) {
        this.accountDao = accountDao;
        this.transferDao = transferDao;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transfer(Transfer transferRequest) {
        //This needs to be handled as a single transaction
        if (transferRequest.getValue().compareTo(BigDecimal.ZERO) < 0) {

            throw new BadRequestException("Value negative");
        }

        Account sourceAccount = findSafely(transferRequest.getSource());

        if (transferRequest.getValue().compareTo(sourceAccount.getBalance()) > 0) {
            throw new BadRequestException("Balance insufficient");
        }

        Account destinationAccount = findSafely(transferRequest.getDestination());

        sourceAccount.setBalance(sourceAccount.getBalance().subtract(transferRequest.getValue()));
        destinationAccount.setBalance(destinationAccount.getBalance().add(transferRequest.getValue()));

        accountDao.update(sourceAccount);
        accountDao.update(destinationAccount);

        transferDao.create(transferRequest);

        return Response.ok().build();
    }

    private Account findSafely(UUID accountId) {
        return accountDao.findById(accountId).orElseThrow(() -> new NotFoundException("Account not found"));
    }
}
