package org.bitbucket.retbrown.revolut.account.db;

import org.bitbucket.retbrown.revolut.account.entity.Transfer;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class TransferDao {
    //This can be replaced with a full in memory database in larger example
    private Map<UUID, Transfer> transferMap = new HashMap<>();

    public Optional<Transfer> findById(UUID id) {
        return Optional.ofNullable(transferMap.get(id));
    }

    public Transfer create(Transfer transfer) {
        return transferMap.put(transfer.getId(), transfer);
    }
}
