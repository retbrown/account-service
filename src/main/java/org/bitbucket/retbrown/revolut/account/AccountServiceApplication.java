package org.bitbucket.retbrown.revolut.account;

import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.bitbucket.retbrown.revolut.account.db.AccountDao;
import org.bitbucket.retbrown.revolut.account.db.TransferDao;
import org.bitbucket.retbrown.revolut.account.resources.TransferResource;


public class AccountServiceApplication extends Application<AccountServiceConfiguration> {
    public static void main(String[] args) throws Exception {
        new AccountServiceApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<AccountServiceConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );
    }

    @Override
    public void run(AccountServiceConfiguration configuration, Environment environment) {
        final AccountDao accountDao = new AccountDao();
        final TransferDao transferDao = new TransferDao();

        environment.jersey().register(new TransferResource(accountDao, transferDao));
    }
}
