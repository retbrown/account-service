package org.bitbucket.retbrown.revolut.account.integration;

import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.bitbucket.retbrown.revolut.account.AccountServiceApplication;
import org.bitbucket.retbrown.revolut.account.AccountServiceConfiguration;
import org.bitbucket.retbrown.revolut.account.entity.Transfer;
import org.junit.ClassRule;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class IntegrationTest {

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test-config.yml");

    @ClassRule
    public static final DropwizardAppRule<AccountServiceConfiguration> RULE = new DropwizardAppRule<>(
            AccountServiceApplication.class, CONFIG_PATH);

    @Test
    public void transfer_ValueIsPositiveAndLessThanBalance() {
        Transfer transfer = new Transfer(UUID.randomUUID(), UUID.fromString("538978eb-cebe-4383-afd2-1310e569b0bd"), UUID.fromString("b923ad4f-0dca-46be-be67-afb69b722787"), new BigDecimal(10.00));

        final Response transferResult = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfer")
                .request()
                .post(Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
        assertThat(transferResult.getStatus()).isEqualTo(200);
    }

    @Test
    public void transfer_ValueIsNegative() {
        Transfer transfer = new Transfer(UUID.randomUUID(), UUID.fromString("538978eb-cebe-4383-afd2-1310e569b0bd"), UUID.fromString("b923ad4f-0dca-46be-be67-afb69b722787"), new BigDecimal(-10.00));

        final Response transferResult = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfer")
                .request()
                .post(Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
        assertThat(transferResult.getStatus()).isEqualTo(400);
        assertThat(transferResult.readEntity(String.class)).isEqualTo("{\"code\":400,\"message\":\"Value negative\"}");
    }

    @Test
    public void transfer_ValueIsPositiveAndGreaterThanBalance() {
        Transfer transfer = new Transfer(UUID.randomUUID(), UUID.fromString("538978eb-cebe-4383-afd2-1310e569b0bd"), UUID.fromString("b923ad4f-0dca-46be-be67-afb69b722787"), new BigDecimal(100.00));

        final Response transferResult = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfer")
                .request()
                .post(Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
        assertThat(transferResult.getStatus()).isEqualTo(400);
        assertThat(transferResult.readEntity(String.class)).isEqualTo("{\"code\":400,\"message\":\"Balance insufficient\"}");
    }

    @Test
    public void transfer_SourceAccountNotFound() {
        Transfer transfer = new Transfer(UUID.randomUUID(), UUID.fromString("538928eb-cebe-4383-afd2-1310e569b0bd"), UUID.fromString("b923ad4f-0dca-46be-be67-afb69b722787"), new BigDecimal(10.00));

        final Response transferResult = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfer")
                .request()
                .post(Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
        assertThat(transferResult.getStatus()).isEqualTo(404);
        assertThat(transferResult.readEntity(String.class)).isEqualTo("{\"code\":404,\"message\":\"Account not found\"}");
    }

    @Test
    public void transfer_DestinationAccountNotFound() {
        Transfer transfer = new Transfer(UUID.randomUUID(), UUID.fromString("f203fcd4-e4e0-4922-afe5-8197b8bd3360"), UUID.fromString("c923ad4f-0dca-46be-be67-afb69b722787"), new BigDecimal(10.00));

        final Response transferResult = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfer")
                .request()
                .post(Entity.entity(transfer, MediaType.APPLICATION_JSON_TYPE));
        assertThat(transferResult.getStatus()).isEqualTo(404);
        assertThat(transferResult.readEntity(String.class)).isEqualTo("{\"code\":404,\"message\":\"Account not found\"}");
    }
}
